# Iron integration test example

This is an example of a very simple web service written in Rust using the Iron framework.
It also shows how to structure the project to get integration tests that exercise the request handling as well as the internal logic.

Blog post: https://www.nibor.org/blog/integration-testing-a-service-written-in-rust-and-iron/

## License

MIT License
