extern crate iron_testing_example;

use iron_testing_example::start_server;

fn main() {
    start_server("0.0.0.0", 3000);
}
