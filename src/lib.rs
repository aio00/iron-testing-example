extern crate iron;
extern crate urlencoded;

use iron::prelude::*;
use iron::status;
use urlencoded::UrlEncodedQuery;

pub fn start_server(host: &str, port: u16) -> iron::Listening {
    Iron::new(handler)
        .http((host, port))
        .expect("Unable to start server")
}

fn handler(request: &mut Request) -> IronResult<Response> {
    let query = request.get_ref::<UrlEncodedQuery>().unwrap();
    if let Some(str_params) = query.get("str") {
        if let Some(len_params) = query.get("len") {
            let s = &str_params[0];
            let len = len_params[0].parse().expect("Error parsing len query param");
            let padded = leftpad(s, len);
            return Ok(Response::with((status::Ok, padded)));
        }
    }
    return Ok(Response::with((status::BadRequest, "Specify str and len query params")));
}

fn leftpad(s: &str, len: usize) -> String {
    format!("{: >width$}", s, width = len)
}

#[test]
fn test_leftpad() {
    assert_eq!(leftpad("foo", 2), "foo".to_string());
    assert_eq!(leftpad("foo", 4), " foo".to_string());
    assert_eq!(leftpad("foo", 6), "   foo".to_string());
    assert_eq!(leftpad("", 4), "    ".to_string());
}
